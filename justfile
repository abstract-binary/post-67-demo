default:
	just --choose

# check format and compilation
check:
	cargo fmt --all
	cargo check --workspace

# run all tests
test:
	cargo test --workspace

# debug build
debug: check
	cargo build

# release build
release: check
	cargo build --release --workspace

# clean files unknown to git
clean:
	git clean -dxf

# run clippy
clippy:
	cargo clippy --workspace
