fn main() -> eyre::Result<()> {
    use crate::generalizing_with_macro_rules::*;
    const EUR: Currency = Currency::new_unchecked([b'E', b'U', b'R', 0], 3);
    let event = handle_event("ACC1".parse()?, 100.0, EUR, "test trade")?;
    println!("{:?}", event);
    println!("{}", event.currency);
    println!("{:?}", "BITCOIN".parse::<Currency>());
    Ok(())
}

#[allow(dead_code)]
pub mod not_even_going_to_bother {
    use log::info;

    #[derive(Debug)]
    pub struct Event {
        pub account: String,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: String,
        pub narrative: String,
    }

    pub fn handle_event(acc: &str, net_cash: f64, cur: &str, narrative: &str) -> Event {
        info!(
            "Account {} {}ed with {} {}: {}",
            acc,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            cur,
            narrative
        );
        Event {
            account: acc.into(),
            net_cash,
            gbp_cash: fx_convert(net_cash, cur, "GBP"),
            currency: cur.into(),
            narrative: narrative.into(),
        }
    }

    pub fn fx_convert(amount: f64, _cur1: &str, _cur2: &str) -> f64 {
        amount * 1.2
    }
}

#[allow(dead_code)]
pub mod enum_list {
    use eyre::bail;
    use log::info;
    use std::{fmt, str};

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub enum Currency {
        GBP,
        EUR,
    }

    impl fmt::Display for Currency {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Currency::GBP => write!(f, "GBP"),
                Currency::EUR => write!(f, "EUR"),
            }
        }
    }

    impl str::FromStr for Currency {
        type Err = eyre::Error;

        fn from_str(str: &str) -> Result<Self, Self::Err> {
            match str {
                "EUR" => Ok(Currency::EUR),
                "GBP" => Ok(Currency::GBP),
                _ => bail!("Unknown currency: '{str}'"),
            }
        }
    }

    #[derive(Debug)]
    pub struct Event {
        pub account: String,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: Currency,
        pub narrative: String,
    }

    pub fn handle_event(acc: &str, net_cash: f64, cur: Currency, narrative: &str) -> Event {
        info!(
            "Account {} {}ed with {} {}: {}",
            acc,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            cur,
            narrative
        );
        Event {
            account: acc.into(),
            net_cash,
            gbp_cash: fx_convert(net_cash, cur, Currency::GBP),
            currency: cur,
            narrative: narrative.into(),
        }
    }

    pub fn fx_convert(amount: f64, _cur1: Currency, _cur2: Currency) -> f64 {
        amount * 1.2
    }
}

#[allow(dead_code)]
pub mod type_alias {
    use log::info;

    type Currency = String;

    #[derive(Debug)]
    pub struct Event {
        pub account: String,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: Currency,
        pub narrative: String,
    }

    pub fn handle_event(acc: &str, net_cash: f64, cur: &Currency, narrative: &str) -> Event {
        info!(
            "Account {} {}ed with {} {}: {}",
            acc,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            cur,
            narrative
        );
        Event {
            account: acc.into(),
            net_cash,
            gbp_cash: fx_convert(net_cash, cur, &"GBP".to_string()),
            currency: cur.into(),
            narrative: narrative.into(),
        }
    }

    pub fn fx_convert(amount: f64, _cur1: &Currency, _cur2: &Currency) -> f64 {
        amount * 1.2
    }
}

#[allow(dead_code)]
pub mod fixed_size_reference {
    use log::info;
    use std::fmt;

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub struct Currency<'a>(&'a str);

    impl<'a> fmt::Display for Currency<'a> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.0)
        }
    }

    impl<'a> From<&'a str> for Currency<'a> {
        fn from(str: &'a str) -> Self {
            Self(str)
        }
    }

    // More impls here: Serialize/Deserialize, ToSql/FromSql,
    // From<String>, From<&String>, etc.

    #[derive(Debug)]
    pub struct Event<'a> {
        pub account: String,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: Currency<'a>,
        pub narrative: String,
    }

    pub fn handle_event<'a>(
        acc: &str,
        net_cash: f64,
        cur: Currency<'a>,
        narrative: &str,
    ) -> Event<'a> {
        info!(
            "Account {} {}ed with {} {}: {}",
            acc,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            cur,
            narrative
        );
        Event {
            account: acc.into(),
            net_cash,
            gbp_cash: fx_convert(net_cash, cur, "GBP".into()),
            currency: cur,
            narrative: narrative.into(),
        }
    }

    pub fn fx_convert(amount: f64, _cur1: Currency, _cur2: Currency) -> f64 {
        amount * 1.2
    }
}

#[allow(dead_code)]
pub mod newtype_wrapper {
    use log::info;
    use std::fmt;

    #[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub struct Currency(String);

    impl fmt::Display for Currency {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.0)
        }
    }

    impl From<&str> for Currency {
        fn from(str: &str) -> Self {
            Self(str.to_string())
        }
    }

    // More impls here: Serialize/Deserialize, ToSql/FromSql,
    // From<String>, From<&String>, etc.

    #[derive(Debug)]
    pub struct Event {
        pub account: String,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: Currency,
        pub narrative: String,
    }

    pub fn handle_event(acc: &str, net_cash: f64, cur: &Currency, narrative: &str) -> Event {
        info!(
            "Account {} {}ed with {} {}: {}",
            acc,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            cur,
            narrative
        );
        Event {
            account: acc.into(),
            net_cash,
            gbp_cash: fx_convert(net_cash, cur, &"GBP".into()),
            currency: cur.clone(),
            narrative: narrative.into(),
        }
    }

    pub fn fx_convert(amount: f64, _cur1: &Currency, _cur2: &Currency) -> f64 {
        amount * 1.2
    }
}

#[allow(dead_code)]
pub mod fixed_size_const_generics {
    use eyre::Context;
    use log::info;
    use std::{fmt, str};

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub struct FixedStr<const N: usize> {
        buf: [u8; N],
    }

    impl<const N: usize> FixedStr<N> {
        pub fn as_str(&self) -> &str {
            // By construction, this should never fail
            str::from_utf8(&self.buf[..]).expect("invalid utf8 in FixedStr")
        }
    }

    impl<const N: usize> fmt::Display for FixedStr<N> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.as_str())
        }
    }

    impl<const N: usize> str::FromStr for FixedStr<N> {
        type Err = eyre::Error;

        fn from_str(str: &str) -> Result<Self, Self::Err> {
            // This fails if `str` isn't exactly `N` bytes long.
            Ok(Self {
                buf: str.as_bytes().try_into()?,
            })
        }
    }

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub struct Currency(FixedStr<3>);

    impl fmt::Display for Currency {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.0.as_str())
        }
    }

    impl str::FromStr for Currency {
        type Err = eyre::Error;

        fn from_str(str: &str) -> Result<Self, Self::Err> {
            // This fails if `str` isn't exactly 3 bytes long.
            Ok(Self(
                str.parse().wrap_err("converting '{str}' into Currency")?,
            ))
        }
    }

    // More impls here: Serialize/Deserialize, ToSql/FromSql,
    // TryFrom<String>, TryFrom<&String>, etc.

    #[derive(Debug)]
    pub struct Event {
        pub account: String,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: Currency,
        pub narrative: String,
    }

    pub fn handle_event(
        acc: &str,
        net_cash: f64,
        cur: Currency,
        narrative: &str,
    ) -> eyre::Result<Event> {
        info!(
            "Account {} {}ed with {} {}: {}",
            acc,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            cur,
            narrative
        );
        Ok(Event {
            account: acc.into(),
            net_cash,
            gbp_cash: fx_convert(net_cash, cur, "GBP".parse()?),
            currency: cur,
            narrative: narrative.into(),
        })
    }

    pub fn fx_convert(amount: f64, _cur1: Currency, _cur2: Currency) -> f64 {
        amount * 1.2
    }
}

#[allow(dead_code)]
pub mod max_size_const_generics {
    use eyre::{bail, Context};
    use log::info;
    use std::{fmt, str};

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub struct StackStr<const N: usize> {
        len: u8,
        buf: [u8; N],
    }

    impl<const N: usize> StackStr<N> {
        pub fn as_str(&self) -> &str {
            // By construction, this should never fail
            str::from_utf8(&self.buf[..self.len as usize]).expect("invalid utf8 in FixedStr")
        }
    }

    impl<const N: usize> fmt::Display for StackStr<N> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.as_str())
        }
    }

    impl<const N: usize> str::FromStr for StackStr<N> {
        type Err = eyre::Error;

        fn from_str(str: &str) -> Result<Self, Self::Err> {
            let bytes = str.as_bytes();
            let len = bytes.len();
            if len > u8::MAX as usize {
                bail!("StackStr can be at most {} big", u8::MAX);
            }
            if len >= N {
                bail!("String '{str}' does not fit in StackStr<{N}>");
            }
            let mut buf = [0; N];
            buf.as_mut_slice()[0..len].copy_from_slice(bytes);
            Ok(Self {
                len: len as u8,
                buf,
            })
        }
    }

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub struct Currency(StackStr<4>);

    impl fmt::Display for Currency {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.0.as_str())
        }
    }

    impl str::FromStr for Currency {
        type Err = eyre::Error;

        fn from_str(str: &str) -> Result<Self, Self::Err> {
            // This fails if `str` isn't exactly 3 bytes long.
            Ok(Self(
                str.parse().wrap_err("converting '{str}' into Currency")?,
            ))
        }
    }

    // More impls here: Serialize/Deserialize, ToSql/FromSql,
    // TryFrom<String>, TryFrom<&String>, etc.

    #[derive(Debug)]
    pub struct Event {
        pub account: String,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: Currency,
        pub narrative: String,
    }

    pub fn handle_event(
        acc: &str,
        net_cash: f64,
        cur: Currency,
        narrative: &str,
    ) -> eyre::Result<Event> {
        info!(
            "Account {} {}ed with {} {}: {}",
            acc,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            cur,
            narrative
        );
        Ok(Event {
            account: acc.into(),
            net_cash,
            gbp_cash: fx_convert(net_cash, cur, "GBP".parse()?),
            currency: cur,
            narrative: narrative.into(),
        })
    }

    pub fn fx_convert(amount: f64, _cur1: Currency, _cur2: Currency) -> f64 {
        amount * 1.2
    }
}

#[allow(dead_code)]
pub mod generalizing_with_macro_rules {
    use eyre::{bail, Context};
    use log::info;
    use std::{fmt, str};

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    pub struct StackStr<const N: usize> {
        len: u8,
        buf: [u8; N],
    }

    impl<const N: usize> StackStr<N> {
        pub const fn new_unchecked(buf: [u8; N], len: u8) -> Self {
            Self { len, buf }
        }

        // This doesn't compile until `const_mut_refs` is stabilized
        // https://github.com/rust-lang/rust/issues/57349
        // pub const fn new_from_str(str: &'static str) -> Self {
        //     let mut buf = [0; N];
        //     unsafe {
        //         std::ptr::copy_nonoverlapping(str.as_ptr(), &mut buf as *mut _, str.len());
        //     }
        //     Self {
        //         len: str.len() as u8,
        //         buf,
        //     }
        // }

        pub fn as_str(&self) -> &str {
            // By construction, this should never fail
            str::from_utf8(&self.buf[..self.len as usize]).expect("invalid utf8 in FixedStr")
        }
    }

    impl<const N: usize> fmt::Display for StackStr<N> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "{}", self.as_str())
        }
    }

    impl<const N: usize> str::FromStr for StackStr<N> {
        type Err = eyre::Error;

        fn from_str(str: &str) -> Result<Self, Self::Err> {
            let bytes = str.as_bytes();
            let len = bytes.len();
            if len > u8::MAX as usize {
                bail!("StackStr can be at most {} big", u8::MAX);
            }
            if len >= N {
                bail!("String '{str}' does not fit in StackStr<{N}>");
            }
            let mut buf = [0; N];
            buf.as_mut_slice()[0..len].copy_from_slice(bytes);
            Ok(Self {
                len: len as u8,
                buf,
            })
        }
    }

    macro_rules! newtype {
        ($name:ident, $size:literal) => {
            #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
            pub struct $name(StackStr<$size>);

            impl $name {
                pub const fn new_unchecked(buf: [u8; $size], len: u8) -> Self {
                    Self(StackStr::new_unchecked(buf, len))
                }
            }

            impl fmt::Display for $name {
                fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    write!(f, "{}", self.0.as_str())
                }
            }

            impl str::FromStr for $name {
                type Err = eyre::Error;

                fn from_str(str: &str) -> Result<Self, Self::Err> {
                    // This fails if `str` is too big
                    Ok(Self(str.parse().wrap_err("converting '{str}' into $name")?))
                }
            }
        };
    }

    newtype!(Currency, 4);
    newtype!(Account, 8);

    const GBP: Currency = Currency::new_unchecked([b'G', b'B', b'P', 0], 3);

    // More impls here: Serialize/Deserialize, ToSql/FromSql,
    // TryFrom<String>, TryFrom<&String>, etc.

    #[derive(Debug)]
    pub struct Event {
        pub account: Account,
        pub net_cash: f64,
        pub gbp_cash: f64,
        pub currency: Currency,
        pub narrative: String,
    }

    pub fn handle_event(
        account: Account,
        net_cash: f64,
        currency: Currency,
        narrative: &str,
    ) -> eyre::Result<Event> {
        info!(
            "Account {} {}ed with {} {}: {}",
            account,
            if net_cash < 0.0 { "debit" } else { "credit" },
            net_cash,
            currency,
            narrative
        );
        Ok(Event {
            account,
            net_cash,
            gbp_cash: fx_convert(net_cash, currency, GBP),
            currency,
            narrative: narrative.into(),
        })
    }

    pub fn fx_convert(amount: f64, _cur1: Currency, _cur2: Currency) -> f64 {
        amount * 1.2
    }
}
