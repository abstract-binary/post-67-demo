post-67-demo
============

> Supporting code for https://scvalex.net/posts/67/

This repo contains several versions of a `Currency` type for Rust,
culminating in one based on const generics that is nice to use.


Build
-----

```
$ nix develop
$ cargo build
```


License
-------

All code under GPLv3.
